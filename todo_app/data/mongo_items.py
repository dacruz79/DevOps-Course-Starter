import requests, os, pymongo

from todo_app.Item import Item

list_name = {}

def mongo_connection_string():
    return os.getenv("CONNECTION_STRING")

def mongo_database():
    return os.getenv("MONGO_DB_NAME")

def mongo_collection():
    return os.getenv("MONGO_COLLECTION_NAME")

def get_board_list():
    """
    List of statuses

    Returns:
        name: The name of the item.
        id:   The id of the items.
    """

    list_name = [{'id':"To Do",'name':"To Do"},{'id':"Doing",'name':"Doing"},{'id':"Done",'name':"Done"}]
    return list_name

def get_to_do_items():
    """
    Fetches all items from Mongo database.

    Returns:
        list: The list of items.
    """
    client = pymongo.MongoClient(mongo_connection_string())
    database = client[mongo_database()]
    collection = database[mongo_collection()]
    cursor = collection.find({})

    obj_list = []
    for document in cursor:
        obj_list.append(Item.from_trello_card(document))

    return obj_list 


def add_item_to_list(title, description, list_status = 'To Do'):
    """
    Adds a new item with the specified title to Mongo database.

    Args:
        title:        The title of the item.
        description:  The description of the item.
        list_status:  List to add item to, defaulted to To Do
    Returns:
        document: The saved document.
    """

    document = {
        'name': title,
        'status': list_status
    }
    
    client = pymongo.MongoClient(mongo_connection_string())
    database = client[mongo_database()]
    collection = database[mongo_collection()]
    collection.insert_one(document)

def change_list_of_item(card_id, list_status):
    """
    Change the list the item should fall in Mongo database.

    Args:
        card_id:     The id of the item.
        list_status: The name of the list to go to.
    Returns:
        item: The saved item.
    """
    client = pymongo.MongoClient(mongo_connection_string())
    database = client[mongo_database()]
    collection = database[mongo_collection()]
    collection.update_one({"_id":card_id},{"$set":{'status':list_status}})
