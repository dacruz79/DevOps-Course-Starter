variable "prefix" {
  description = "The prefix used for all resources in this environment"
  default = "nouvel-tf-todoapp"
}

variable "mongo_db_name" {
  type    = string
  default = "nouvel_todoapp_db"
}

variable "mongo_collection_name" {
  type    = string
  default = "ToDo_Task"
}

variable "flask_app" {
  type    = string
  default = "todo_app/app"
}

variable "secret_key" {
  type    = string
  sensitive = true
}
