import os
from dotenv import load_dotenv, find_dotenv
import pytest
import requests
import pymongo
import mongomock

from todo_app import app

@pytest.fixture
def client():
    # Use our test integration config instead of the 'real' version
    file_path = find_dotenv('.env.test')
    load_dotenv(file_path, override=True)

    with mongomock.patch(servers=(('fakemongo.com', 27017),)):
        test_app = app.create_app()
        with test_app.test_client() as client:
            yield client

def test_index_page(client):
    document = {'name': 'Test Item', 'status': 'To Do'}

    mongo_client    = pymongo.MongoClient(os.getenv("CONNECTION_STRING"))
    database        = mongo_client[os.getenv("MONGO_DB_NAME")]
    collection      = database[os.getenv("MONGO_COLLECTION_NAME")]

    collection.insert_one(document)
    response = client.get('/')
    print(response.data.decode())

    assert response.status_code == 200
    assert 'Test Item' in response.data.decode()
