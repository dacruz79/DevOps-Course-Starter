# DevOps Apprenticeship: Project Exercise

> If you are using GitPod for the project exercise (i.e. you cannot use your local machine) then you'll want to launch a VM using the [following link](https://gitpod.io/#https://github.com/CorndelWithSoftwire/DevOps-Course-Starter). Note this VM comes pre-setup with Python & Poetry pre-installed.

## System Requirements

The project uses poetry for Python to create an isolated environment and manage package dependencies. To prepare your system, ensure you have an official distribution of Python version 3.8+ and install Poetry using one of the following commands (as instructed by the [poetry documentation](https://python-poetry.org/docs/#system-requirements)):

### Poetry installation (Bash)

```bash
curl -sSL https://install.python-poetry.org | python3 -
```

### Poetry installation (PowerShell)

```powershell
(Invoke-WebRequest -Uri https://install.python-poetry.org -UseBasicParsing).Content | py -
```

## Dependencies

The project uses a virtual environment to isolate package dependencies. To create the virtual environment and install required packages, run the following from your preferred shell:

```bash
$ poetry install
```

You'll also need to clone a new `.env` file from the `.env.template` to store local configuration options. This is a one-time operation on first setup:

```bash
$ cp .env.template .env  # (first time only)
```

You will need to create a Trello account and generate and API Key and Token. Follow the instructions in the below links:
    [https://trello.com/signup]
    [https://trello.com/app-key]

Once you have an account, create some cards (at least one in To Do list.)

The `.env` file is used by flask to set environment variables when running `flask run`. This enables things like development mode (which also enables features like hot reloading when you make a file change). There's also a [SECRET_KEY](https://flask.palletsprojects.com/en/1.1.x/config/#SECRET_KEY) variable which is used to encrypt the flask session cookie.

Set the following value in the `'.env` file once you have got these from Trello.
    [API_KEY=]
    [API_TOKEN=]

To get the Board Id:
```bash
    * Go to your Trello Board
    * Add ".json" to the end of the URL
    * Format the JSON so it is readable, search for idBoard OR
    * the first value is normally the id {"id":"....
```
    [BOARD_ID=]

## Running the App

Once the all dependencies have been installed, start the Flask app in development mode within the Poetry environment by running:
```bash
$ poetry run flask run
```

You should see output similar to the following:
```bash
 * Serving Flask app "app" (lazy loading)
 * Environment: development
 * Debug mode: on
 * Running on http://127.0.0.1:5000/ (Press CTRL+C to quit)
 * Restarting with fsevents reloader
 * Debugger is active!
 * Debugger PIN: 226-556-590
```
Now visit [`http://localhost:5000/`](http://localhost:5000/) in your web browser to view the app.

You will be able to see:
```bash
 * All your tasks
 * If you want to move a task to Done hit the Complete button
 * Add new tasks to a list you have available on Trello board i.e. To Do, Doing, Done.
``` 

## Testing

To run all tests configured, where files are test_*py or *_test.py do so by running:
```bash
$ poetry run pytest
```

You should see either all tests PASSED or some FAILED:
```bash
 * ====================================================================================================================== short test summary info====================================================================================================================== 
 * FAILED todo_app/test_view_model.py::test_view_model_to_do_items - AssertionError: assert 'Done' == 'To Do'
 * ==================================================================================================================== 1 failed, 1 passed in 9.24s===================================================================================================================
```

## Virtual Machines

To ssh to a VM
```bash
$ ssh USERNAME@IP-ADDRESS
```

When using ansible to deploy changes to different VM's we need to install ansible.

```bash
$ ansible --version
```

If not already installed
```bash
$ sudo pip install ansible
```

What you may find if you cannot ssh to the VM's as ssh public key is not set up.  
You will need to create a key first which will add a public key into .ssh directory under your home dir.

```bash
$ ssh-keygen
```

Copy key from the .ssh dir using the following:

```bash
$ ssh-copy-id <user>@<hostname>
```

## Ansible

To run the ansible playbook - on control node:

```bash
$ ansible-playbook my-ansible-playbook.yml -i my-ansible-inventory.ini
```

## Docker

For local runs Docker Desktop needs to be started. 
If you do not have it please install from: https://www.docker.com/products/docker-desktop

To build the containers we will need to run the following.

Development uses Flask
```bash
$ docker build --target development --tag todo-app:dev .
```

Production uses gunicorn
```bash
$ docker build --target production --tag todo-app:prod .
```

To use the built containers for the run:

## --mount "type=bind,source=$(pwd)/todo_app,target=/todo_app/todo_app"
Development:
```bash
$ docker run --env-file ./.env -p 5000:5000 todo-app:dev
```
Production:
```bash
$ docker run --env-file .env -p 5000:5000 todo-app:prod
```
A better way to run docker is to use docker compose as this will have all the 
instructions inside it so no need to pass variables like --env-file,etc.

For now there are two file compose.yaml and compose-dev.yaml but should be one.

Development:
```bash
$ docker-compose -f .\compose-dev.yaml up 
```

Production:
```bash
$ docker-compose -f .\compose.yaml up 
```

You can click on the link to seethe results when run successfully
http://127.0.0.1:5000/


## Using AZURE Cloud

Go to portal.azure.com and create an account.

You will need a resouce group in Azure portal to continue.

# Install Azure Client
Follow the instructions at: https://docs.microsoft.com/en-us/cli/azure/install-azure-cli to install the CLI on your machine if you haven’t already.

```bash
$ az login
```

# Push Container to Docker Hub Registry
Login to Docker
```bash
$ docker login
```

Build the image
```bash
$ docker build --target production --tag <docker_username>/todo_app:latest .
```

Push image to Docker Hub
```bash
$ docker push <docker_username>/todo_app:latest
```

# Create an Azure Web App
From Azure Portal:
    Create a Resource -> Web App
    Select your Project Exercise resource group.
    In the “Publish” field, select “Docker Container”
    Under “App Service Plan”, it should default to creating a new one, which is correct. Just change the “Sku and size” to “B1”.
    On the next screen, select Docker Hub in the “Image Source” field, and enter the details of your image i.e. <docker_username>/todo_app:latest

Manually using Client:

First create an App Service Plan: 
```bash
$ az appservice plan create --resource-group <resource_group_name> -n <appservice_plan_name> --sku B1 --is-linux
```

Then create the Web App: 
```bash
$ az webapp create --resource-group <resource_group_name> --plan <appservice_plan_name> --name <webapp_name> --deployment-container-image-name docker.io/<dockerhub_username>/todo_app:latest
```

# Set up Environment Variables in Azure

From Azure Portal:
    Settings -> Configuration in the Portal OR
    Settings -> Environment variables

Manually using Client:
```bash
$ az webapp config appsettings set -g <resource_group_name> -n <webapp_name> --settings FLASK_APP=todo_app/app.
```

Or you can pass in a JSON file containing all variables by using --settings @foo.json

Variables needed:  API_KEY; API_TOKEN; BOARD_ID; FLASK_APP; SECRET_KEY etc as part of .env file. You will also need to add your Docker username and password in CI_USER and CI_PASSWORD.

Finally add your webhook to variable AZURE_WEBHOOK, this will allow any changes done to be automatically reflected in the Web App. 
From Azure Portal:
    Web App -> Deployment -> Deployment Center

# Using the Vault
From the Azure Portal:
    - Create a resource -> Key Vault
Link to your resource group:
    - Permission model -> Vault access policy
    - Firewalls and virtual network -> Access allowed from all networks
To set Secrets:
    - <Vault> -> Objects -> Secrets

Finally once your Secrets are created you can update your Environment Variables to use this by replacing the Value to have a string as below:
    - @Microsoft.KeyVault(VaultName=<vault name>;SecretName=<secret name>)

# Check it all works
Browse to http://<webapp_name>.azurewebsites.net/
For this project we are using: https://noubarwebapp.azurewebsites.net/

# Creating a MongoDB

```bash
$ poetry add mongomock
$ poetry add pymongo 
```

From Azure Portal: 
    New -> CosmosDB Database
    Select “Azure Cosmos DB API for MongoDB”
    Choose “Serverless” for Capacity mode
    You can also configure secure firewall connections here, but for now you should permit access from “All Networks” to enable easier testing of the integration with the app.

Manually using Client:
```bash
$ az cosmosdb create --name nouveldevops --resource-group Cohort29_NouBar_ProjectExercise --kind MongoDB -
-capabilities EnableServerless --server-version 4.2
$ az cosmosdb create --name <cosmos_account_name> --resource-group <resource_group_name> --kind MongoDB --capabilities EnableServerless --server-version 3.6

$ az cosmosdb mongodb database create --account-name nouveldevops --name nouvel_todoapp_db --resource-group Cohort29_NouBar_ProjectExercise
$ az cosmosdb mongodb database create --account-name <cosmos_account_name> --name <database_name> --resource-group <resource_group_name>
```

Variables needed in .env (and Environment variables for Web App):
CONNECTION_STRING --> get this from Azure - CosmosDB --> Settings --> Connection strings --> PRIMARY CONNECTION STRING
MONGO_DB_NAME
MONGO_COLLECTION_NAME

Checking it all works:
```bash
$ import pymongo

$ client = pymongo.MongoClient("mongodb://nouveldevops:<password>==@nouveldevops.mongo.cosmos.azure.com:10255/?ssl=true&replicaSet=globaldb&retrywrites=false&maxIdleTimeMS=120000&appName=@nouveldevops@")

$ client.list_database_names()
```

## Data and Security

# Encryption at rest and in transit
With Azure Cosmos DB, all your databases, media attachments, and backups are encrypted. Your data is now encrypted in transit (over the network) and at rest (nonvolatile storage), giving you end-to-end encryption.

Under Web App --> Settings --> Configuration 
You will see that there is a default HTTPS Only set on which means you will always be routed to https:// which handles encryption in transit.
Additionally you can see that in the CONNECTION_STRING we have it has ssl=true which esures that data between the application and the Cosmos DB will be encrypted in transit.  This is a default setting.

# Dependancy Checking

There are many tools for scanning our dependencies; here we’ll use safety which is freely available for non-commercial use only.

Install through poetry
```bash
$ poetry add safety
```

Check dependancies locally by ruuning:
```bash
$ poetry run safety check
```

This will produce a simple report noting any vulnerable libraries. For the sake of this exercise it is not required to resolve all warnings, although it is obviously a good idea. This is often as simple as asking Poetry to find us a newer version:
```bash
$ poetry update <library>
```

## Infrastructure as Code
# Terraform running locally
HashiCorp Terraform is an infrastructure as code tool that lets you define both cloud and on-prem resources in human-readable configuration files that you can version, reuse, and share.

Update the main.tf with your subscription id which you can get from the below looking at the "id" string
```bash
$ az group show --name <RESOURCE GROUP NAME> 
```

First initialise the directory(.terraform), setup backend and link the azurerm
```bash
$ terraform init
```

To see what actions would be performed you can run the following:
```bash
$ terraform plan
```

To add any resources you can run, you will need to approve the changes:
```bash
$ terraform apply
```

To remove any resources you created:
```bash
$ terraform destroy
```

Other useful commands to vier current state is as expected:
```bash
$ terraform state list
$ terraform show
```

variables.tf has the key variables
To pass in variables at command line we can do this:
```bash
$ terraform apply -var 'prefix=test' -var 'mongo_db_name='xxx''
```

# Parametrise for different environments
You will need to create a file variables.tf with the following:

variable "prefix" {
  description = "The prefix used for all resources in this environment"
  default = "<>"
}

variable "mongo_db_name" {
  type    = string
  default = "<>"
}

variable "mongo_collection_name" {
  type    = string
  default = "<>"
}

variable "flask_app" {
  type    = string
  default = "<>"
}

variable "secret_key" {
  type    = string
  sensitive = true
  default = "<>"
}

and output.tf with the following:
output "webapp_url" {
  value = "https://${azurerm_linux_web_app.main.default_hostname}"
}

output "cd_webhook" {
  value = "https://${azurerm_linux_web_app.main.site_credential[0].name}:${azurerm_linux_web_app.main.site_credential[0].password}@${azurerm_linux_web_app.main.name}.scm.azurewebsites.net/api/registry/webhook"
  sensitive = true
}


# Using Blob storage as Remote State
To store the state as an encrypted Blob we need to create a storage account and container
```bash
$ az storage account create --resource-group Cohort29_NouBar_ProjectExercise --name nbtodoappstorageacct --sku Standard_LRS --encryption-services blob
$ az storage account create --resource-group <resource_group_name> --name <storage_account_name> --sku Standard_LRS --encryption-services blob

$ az storage container create --name nouvel-todoapp-container --account-name nbtodoappstorageacct
$ az storage container create --name <container_name> --account-name <storage_account_name>
```

To list your azure account list run (id is the subscription ID):
```bash
$ az account list
```
If you have more than one ID you have to tell Azure CLI which to use by:
```bash
$ az account set --subscription="<id>"
```

# Service Pprincipal Authentication
To create a new Service Principal run (this will create an app registration with you as the owner):
```bash
$ az ad sp create-for-rbac --name "nouvel_todoapp_service_principal" --role Contributor --scopes /subscriptions/d33b95c7-af3c-4247-9661-aa96d47fccc0/resourceGroups/Cohort29_NouBar_ProjectExercise
$ az ad sp create-for-rbac --name "<SERVICE PRINCIPAL NAME>" --role Contributor --scopes /subscriptions/<SUBSCRIPTION ID>/resourceGroups/<RESOURCE GROUP NAME>"
```
Add the following environment variables to your pipeline (securely): ARM_CLIENT_ID, ARM_CLIENT_SECRET, ARM_TENANT_ID, ARM_SUBSCRIPTION_ID. You can find the correct values for the first three from the newly created Service Principal.  On client search for "App registrations".

Either set values for your input variables with -var tags, or set environment variables. E.g. an environment variable called “TF_VAR_client_secret” would automatically get used for a terraform input variable called “client_secret”.
